import Header from "./components/Header";
import Page from "./components/Page";

function App() {
  return (
    <div className="relative w-full">
      <a className="group flex items-center transition-height delay-200 h-10 bg-primary hover:bg-primary-400">
        <p className="text-sky-darkest group-hover:text-black sm:top-1.5 w-full pl-4 sm:pl-0 sm:text-center text-sm"></p>
      </a>
      <Header />
      <Page />
    </div>
  );
}

export default App;
