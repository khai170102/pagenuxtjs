import React from "react";
import { NAV } from "../../../data/data";
import { BiChevronDown } from "react-icons/bi";

function Nav() {
  return (
    <nav className="items-center justify-center hidden h-full lg:flex">
      {NAV.map((items, index) => {
        return (
          <div
            key={index}
            className="  relative inline-block text-left cursor-default"
          >
            <div className="flex items-center w-full mx-2 group">
              <span className="flex font-medium group whitespace-nowrap px-1 py-2 hover:text-gray-300 text-white">
                {items.title}
              </span>
              <BiChevronDown className="h-4 w-4 text-white" />
              <div
                className=" bg-[rgb(2,42,53)] left-[-79px] w-[300px] 
              top-[40px] absolute   invisible  group-hover:visible opacity-100
              z-30 rounded-md shadow-lg  pt-2"
              >
                <div className="py-1 grid grid-cols-1 ">
                  {items.childrend.map((childrend) => {
                    return (
                      <div className="py-1 px-2 rounded-md w-full hover:bg-[rgb(1,30,38)]">
                        <div className="flex space-x-2 items-center">
                          <div className="rounded-md w-8 h-8 p-2  bg-black">
                            <img src={childrend.img} />
                          </div>
                          <div>
                            <h5 className="font-bold text-sm text-white">
                              {childrend.title}
                            </h5>
                            <p className="text-xs text-white">
                              {childrend.des}
                            </p>
                          </div>
                        </div>
                      </div>
                    );
                  })}
                </div>
              </div>
            </div>
          </div>
        );
      })}
      <div className="flex items-center w-full mx-2">
        <span className="flex font-medium group whitespace-nowrap px-1 py-2 hover:text-gray-300 text-white">
          Partners
        </span>
      </div>
    </nav>
  );
}

export default Nav;
