import { IoLanguageSharp } from "react-icons/io5";
import { IoIosArrowDown } from "react-icons/io";
import { HiOutlineSearch } from "react-icons/hi";
import { BsLaptop } from "react-icons/bs";

function Search() {
  return (
    <div className="flex items-center justify-end gap-1 lg:flex-1">
      <div className="group   items-center w-12 h-12 hidden lg:inline-flex relative  text-left cursor-default">
        <IoLanguageSharp className="text-[#D1E1E2] d-icon h-6 w-6  m-auto d-secondary-text hover:text-slate-100" />
        <IoIosArrowDown className="h-4 w-4 text-white " />
      </div>
      <BsLaptop className="h-5 w-[3rem] hidden text-[#D1E1E2]  lg:flex" />

      <HiOutlineSearch className="h-5 w-[3rem] text-[#D1E1E2] " />
    </div>
  );
}

export default Search;
