import Logo from "./components/Logo";
import Nav from "./components/Nav";
import Search from "./components/Search";
import { CgDetailsMore } from "react-icons/cg";

function Header() {
  return (
    <div
      className="w-full fixed z-50 top-0 bg-[rgba(0,30,38)] opacity-[0.8] 
     "
    >
      <div className="flex items-center h-full px-1 py-3 mx-auto max-w-7xl sm:px-3 lg:px-6">
        <button className=" flex items-center transition-colors duration-200 justify-center   w-12 h-12 lg:hidden text-gray-300 hover:text-cloud-lighter">
          <CgDetailsMore className="h-6 w-6" />
        </button>
        <div className="flex items-center flex-1 justify-center lg:justify-start">
          <Logo />
        </div>
        <Nav />
        <Search />
      </div>
    </div>
  );
}

export default Header;
