import React from "react";
import { FOOTERS } from "../../data/data";
import { BsTwitter } from "react-icons/bs";
import { SiDiscord } from "react-icons/si";
import { BsGithub } from "react-icons/bs";

function Footer() {
  return (
    <div className="relative z-10 ">
      <div className="bg-[rgb(1,53,67)]">
        <div className="max-w-7xl mx-auto py-12 px-4 sm:px-6 lg:py-16 lg:px-8 grid gap-8 lg:gap-0 sm:grid-cols-2 lg:grid-cols-4">
          {FOOTERS.map((items) => {
            return (
              <div key={items.title}>
                <h3 className="font-semibold text-lg text-[rgb(145,173,173)]">
                  {items.title}
                </h3>
                <ul className="mt-4 space-y-3">
                  {items.childrend.map((itemChild, index) => {
                    return (
                      <li key={index}>
                        <a className="text-white hover:text-[rgb(145,173,173)] cursor-pointer">
                          {itemChild}
                        </a>
                      </li>
                    );
                  })}
                </ul>
              </div>
            );
          })}
          <div>
            <section>
              <div>
                <h3 className="font-semibold text-lg text-[rgb(145,173,173)]">
                  Newsletter
                </h3>
                <p className="mt-2 text-base text-white ">
                  The latest news, articles, and resources, sent to your inbox
                  monthly.
                </p>
                <form className="mt-4 sm:flex sm:justify-start">
                  <div className="w-[195px] h-[42px] flex items-center rounded-md border border-solid border-[rgb(36,84,96)]">
                    <a className="text-[rgb(161,161,170)] pl-2">Email</a>
                  </div>
                  <button className=" flex mt-2 sm:mt-0 sm:ml-2">
                    <a className="p-[8px] bg-[rgb(0,220,129)] rounded-md">
                      Subscribe
                    </a>
                  </button>
                </form>
              </div>
            </section>
            <ul className="flex items-center space-x-4 xl:space-x-5 mt-4">
              <li>
                <BsTwitter className="h-[25px] w-[25px] text-[rgb(212,212,216)] hover:text-[rgb(0,220,129)]" />
              </li>
              <li>
                <SiDiscord className="h-[25px] w-[25px] text-[rgb(212,212,216)] hover:text-[rgb(0,220,129)]" />
              </li>
              <li>
                <BsGithub className="h-[25px] w-[25px] text-[rgb(212,212,216)] hover:text-[rgb(0,220,129)]" />
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Footer;
//BsTwitter SiDiscord BsGithub
