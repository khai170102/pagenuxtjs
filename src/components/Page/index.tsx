import Footer from "../Footer";
import Banner from "./components/Banner";
import Content from "./components/content";

function Page() {
  return (
    <div className="lg:flex h-screen">
      <div className="flex-auto w-full h-full min-w-0 lg:static lg:max-h-full lg:overflow-visible">
        <div className="bg-white h-full">
          <Banner />
          <Content />
          <Footer />
        </div>
      </div>
    </div>
  );
}

export default Page;
