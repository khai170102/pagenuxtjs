import Community from "./components/Community";
import ContentLearn from "./components/ContentLearn";
import Explore from "./components/Explore";
import Feature from "./components/Feature";
import LearnGuild from "./components/LearnGuild";
import Partners from "./components/Partners";
import Testimonials from "./components/Testimonials";

function Content() {
  return (
    <div>
      <ContentLearn />
      <Feature />
      <Partners />
      <LearnGuild />
      <Explore />
      <Community />
      <Testimonials />
    </div>
  );
}

export default Content;
