import React from "react";
import { TESTIMONIALS } from "../../../../../../data/data";

function Testimonials() {
  return (
    <div className="relative pb-60 lg:pb-80 2xl:pb-102">
      <img
        className="absolute left-0 bottom-0 object-cover w-full"
        src="https://nuxtjs.org/img/home/home_footer.svg"
      />
      <section className="relative">
        <img
          src="https://nuxtjs.org/img/home/testimonials/landscape-community.svg"
          className="w-full h-28 object-cover -mt-12 pointer-events-none"
        />

        <>
          <div className="relative w-full max-w-[80rem] mx-auto px-[1rem] flex flex-col items-center">
            <div className="flex flex-col w-full md:grid md:grid-cols-12">
              <div className="flex flex-col w-full col-span-12 pt-20  items-center jtext-center">
                <div className="mb-2">
                  <span className="opacity-[0.6] font-bold text-lg">
                    Community{" "}
                  </span>
                </div>
                <h2
                  className="font-normal text-center md:text-left 
 font-serif text-4xl  sm:text-4xl 2xl:text-4xl mb-2"
                >
                  Testimonials
                </h2>
                <p className="font-normal text-center md:text-lg 2xl:text-xl line-clamp-2 mb-12">
                  Learn what the experts love about Nuxt.
                </p>
                <div className="w-full my-8">
                  <ul className="grid grid-cols-1 md:grid-cols-3 gap-12 md:gap-4 lg:gap-8 pb-8 ">
                    {TESTIMONIALS.map((items) => {
                      return (
                        <li
                          className="
                          relative
                          flex flex-col
                          items-center
                          justify-between
                          space-y-4
                          border-2 border-gray-200
                          md:transition-all md:ease-out
                          rounded-lg
                          p-4
                        bg-white
                         "
                          key={items.title}
                        >
                          <p className="text-left">{items.des}</p>
                          <div className="flex w-full justify-between items-center cursor-pointer">
                            <img src={items.avatar} className="h-12 w-12" />
                            <a className="flex flex-1 pl-4 text-left flex-col">
                              <span className="font-bold text-base">
                                {items.name}
                              </span>
                              <span className="text-sm text-cloud opacity-[0.6]">
                                {items.title}
                              </span>
                            </a>
                            <img
                              src={items.in}
                              className="w-[28px] h-[28px] hidden xl:block "
                            />
                          </div>
                        </li>
                      );
                    })}
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </>
      </section>
    </div>
  );
}

export default Testimonials;
