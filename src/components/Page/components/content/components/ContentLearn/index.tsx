import React, { useState } from "react";
import imageCode from "../../../../../../assets/imgCode.png";
function ContentLearn() {
  const [select, setSelect] = useState(1);

  const handleClickBtn1 = () => {
    setSelect(1);
  };
  const handleClickBtn2 = () => {
    setSelect(2);
  };

  return (
    <>
      <img
        className="absolute left-0 object-fill w-full h-40 -mt-24"
        src="https://nuxtjs.org/img/home/discover/modules/dark/landscape-discover-modules-t.svg"
      />
      <div className="relative py-20 bg-[rgb(2,42,53)] text-white">
        <div className="relative w-full max-w-[80rem] mx-auto px-[1rem] flex flex-col items-center">
          <div className="flex flex-col w-full md:grid md:grid-cols-12">
            <div className="flex flex-col w-full col-span-12 items-center jtext-center">
              <div className="mb-2">
                <span className="text-[rgb(178,204,204)] font-bold text-lg">
                  Learn
                </span>
              </div>
              <div className="mb-2">
                <h2
                  className="font-normal text-center md:text-left 
 font-serif text-4xl  sm:text-4xl 2xl:text-4xl"
                >
                  <span className="text-[rgb(0,220,129)] mr-2">Easy</span>
                  to learn.
                  <span className="mr-2 ml-2 text-[rgb(0,220,129)]">Easy</span>
                  to master
                </h2>
              </div>
              <div className="mb-8">
                <p className="text-center font-normal text-base md:text-lg 2xl:text-xl line-clamp-2">
                  Learn everything you need to know, from beginner to master.
                </p>
              </div>
              <div className="w-full my-2 sm:my-8 sm:px-[80px]">
                <div className="w-full text-gray-50">
                  <ul className="flex">
                    <li>
                      <button
                        onClick={handleClickBtn1}
                        className="relative mr-8 text-lg font-bold"
                      >
                        From CLI
                        <span
                          className={`${
                            select == 1 ? "opacity-100" : "opacity-0"
                          } absolute bottom-[-0.375rem] left-0 h-[0.125rem] bg-[rgb(0,220,129)] w-1/3`}
                        />
                      </button>
                    </li>
                    <li>
                      <button
                        onClick={handleClickBtn2}
                        className="relative mr-8 text-lg font-bold"
                      >
                        From Scratch
                        <span
                          className={`${
                            select == 2 ? "opacity-100" : "opacity-0"
                          } absolute bottom-[-0.375rem] left-0 h-[0.125rem] bg-[rgb(0,220,129)] w-1/3`}
                        />
                      </button>
                    </li>
                  </ul>

                  <>
                    <div className=":mt-40 mb-40 lg:mt-0 lg:mb-0 w-full h-[300px]">
                      <div className="relative">
                        <div className="grid mt-[2rem] absolute top-0 left-0 grid-cols-12	">
                          <div className="lg:col-span-7 sm:col-span-12 sm:w-full col-span-12 w-full ">
                            <img src={imageCode} />
                          </div>
                          <div
                            className="h-[120%] lg:w-[50%] rounded-md bg-white lg:top-[-14%] absolute lg:left-[50%] 
                            sm:left-0  sm:bottom-[-100%] sm:w-[100%]  left-0  bottom-[-100%] w-[100%] 
                          "
                          >
                            <div className="flex flex-col justify-center items-start h-full px-[4rem]">
                              <img
                                src="https://nuxtjs.org/_nuxt/img/logo.d0868f5.svg"
                                className="mb-[1rem] h-[36px]"
                              />
                              <img
                                src="https://nuxtjs.org/_nuxt/img/cliContent1.7f527cb.svg"
                                className="mb-[1rem]"
                              />
                              <img src="https://nuxtjs.org/_nuxt/img/cliContent2.7191311.svg" />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="w-full self-start ">
                      <a className=" p-2 bg-[rgb(0,220,129)] rounded-md text-black hover:opacity-[0.8]">
                        Start learning
                      </a>
                    </div>
                  </>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <img
        className="absolute left-0 z-10 object-fill w-full h-40 -mt-20"
        src="https://nuxtjs.org/img/home/discover/modules/dark/landscape-discover-modules-b.svg"
      />
    </>
  );
}

export default ContentLearn;
