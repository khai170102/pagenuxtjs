import React from "react";
import { GUIDES } from "../../../../../../data/data";

function LearnGuild() {
  return (
    <div className="relative bg-[rgb(1,30,38)] pt-18 sm:pt-20 md:pt-24 xl:pt-40">
      <div className="relative w-full px-[1rem]  flex flex-col items-center text-white">
        <div className="flex flex-col w-full items-center col-span-12">
          <div className="mb-2">
            <span className="opacity-[0.6] font-bold text-lg">Learn </span>
          </div>
          <h2
            className="font-normal text-center md:text-left 
 font-serif text-4xl  sm:text-4xl 2xl:text-4xl mb-2"
          >
            Follow our {""}
            <span className="text-[rgb(0,220,129)]">Guides</span>
          </h2>
          <p className="font-normal text-center md:text-lg 2xl:text-xl line-clamp-2 mb-12">
            From an idea to a masterpiece, guides take you on the path to
            becoming a Nuxter.
          </p>
          <div className="w-full md:my-8">
            <div className="grid grid-cols-2 sm:grid-cols-4 gap-6 md:gap-8 pb-8">
              <>
                {GUIDES.map((items) => {
                  return (
                    <a
                      className="flex flex-col items-center transition
                     duration-200 p-6 rounded-md hover:bg-[rgb(1,53,67)] cursor-pointer"
                    >
                      <span className="mb-4 transition duration-200  ">
                        <img src={items.img} />
                      </span>
                      <h3 className="mb-1 text-center text-lg lg:text-xl font-bold">
                        {items.title}
                      </h3>
                      <p className="text-center text-sm lg:text-base mb-4">
                        {items.des}
                      </p>
                    </a>
                  );
                })}
              </>
            </div>
          </div>
          ;
        </div>
      </div>
    </div>
  );
}

export default LearnGuild;
