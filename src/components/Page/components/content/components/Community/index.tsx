import React from "react";
import { COMUNITIES } from "../../../../../../data/data";
import { CgChevronRight } from "react-icons/cg";

function Community() {
  return (
    <div className="pb-20 pt-20 bg-[rgb(1,53,67)] text-white">
      <div className="relative w-full max-w-[80rem] px-[1rem] mx-auto ">
        <div className="flex flex-col items-center xl:items-start col-span-12">
          <div className="mb-2">
            <span className="text-[rgb(0,220,129)] font-bold text-lg">
              Community{" "}
            </span>
          </div>
          <h2
            className="font-normal text-center md:text-left 
 font-serif text-4xl  sm:text-4xl 2xl:text-4xl mb-2"
          >
            Sharing is {""}
            <span className="text-[rgb(149,204,222)]">Caring</span>
          </h2>
          <p className="font-normal text-center md:text-lg 2xl:text-xl line-clamp-2 mb-12">
            Discover powerful modules, integrate with your favorite providers
            and start quickly with themes.
          </p>

          <>
            <ul className=" items-start grid grid-cols-1 sm:grid-cols-2 gap-8">
              {COMUNITIES.map((items) => {
                return (
                  <li key={items.title}>
                    <div>
                      <div className="aspect-auto	 bg-gray-100 overflow-hidden mb-4 rounded-lg">
                        <img className="object-fill" src={items.img} />
                      </div>
                      <span className=" text-base text-[rgb(0,220,129)] lg:text-lg font-medium mb-2">
                        {items.name}
                      </span>
                      <h3 className="text-xl lg:text-2xl font-medium mb-2">
                        {items.title}
                      </h3>
                      <p className="mb-4 text-base lg:text-lg truncate">
                        {items.des}
                      </p>
                      <a className="relative inline-flex items-center font-medium group  flex-nowrap max-w-max cursor-pointer ">
                        <span className="text-lg text-[rgb(0,220,129)]">
                          Get infos
                        </span>
                        <CgChevronRight className="ml-2 w-5 h-5 text-[rgb(0,220,129)]" />
                        <span className="absolute -bottom-2 h-0.5 w-8 group-hover:w-full transition-all bg-[rgb(0,220,129)]"></span>
                      </a>
                    </div>
                  </li>
                );
              })}
            </ul>
          </>
        </div>
      </div>
    </div>
  );
}

export default Community;
