import React from "react";
import { FEATURES } from "../../../../../../data/data";

function Feature() {
  return (
    <div className="relative sm:pb-20 lg:pb-28 xl:pb-40 2xl:pb-60">
      <img
        className="absolute left-0 bottom-0 object-cover w-full"
        src="https://nuxtjs.org/img/home/discover/dx/discover-mountain.svg"
      />
      <section className="py-40 bg-white ">
        <div className="relative w-full px-[1rem] flex flex-col items-center">
          <div className="flex flex-col w-full items-center col-span-12">
            <div className="mb-2">
              <span className="opacity-[0.6] font-bold text-lg">Features </span>
            </div>
            <h2
              className="font-normal text-center md:text-left 
 font-serif text-4xl  sm:text-4xl 2xl:text-4xl mb-2"
            >
              Intuitive {""}
              <span className="text-[rgb(0,220,129)]">D</span>
              eveloper E<span className="text-[rgb(0,220,129)]">X</span>
              perience
            </h2>
            <p className="font-normal text-center md:text-lg 2xl:text-xl line-clamp-2 mb-12">
              Nuxt is shipped with plenty of features to boost developer
              productivity and the end user experience.
            </p>

            <>
              <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4">
                {FEATURES.map((items) => {
                  return (
                    <div
                      key={items.title}
                      className="flex flex-col items-center transition duration-200 p-6 rounded-md"
                    >
                      <span className="mb-4 transition duration-200 ">
                        <img className="w-[75px] h-[75px]" src={items.img} />
                      </span>
                      <h3 className="mb-1 text-center text-lg lg:text-xl font-bold">
                        {items.title}
                      </h3>
                      <p className="text-center text-sm lg:text-base mb-4">
                        {items.des}
                      </p>
                    </div>
                  );
                })}
              </div>
            </>
          </div>
        </div>
      </section>
    </div>
  );
}

export default Feature;
