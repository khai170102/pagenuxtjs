import React from "react";
import { EXPLORES } from "../../../../../../data/data";

function Explore() {
  return (
    <div className="relative pb-20 sm:pb-28 md:pb-36 lg:pb-48">
      <section className="relative z-10">
        <img
          className="left-0 bottom-0 object-cover w-full h-40 -mt-16"
          src="https://nuxtjs.org/img/home/explore/landscape-explore.svg"
        />

        <div className="relative w-full px-[1rem] flex flex-col items-center py-20 t">
          <div className="flex flex-col items-center w-full col-span-12">
            <div className="mb-2">
              <span className="opacity-[0.6] font-bold text-lg">Explore </span>
            </div>
            <h2
              className="font-normal text-center md:text-left 
 font-serif text-4xl  sm:text-4xl 2xl:text-4xl mb-2"
            >
              Moving forward? So much to {""}
              <span className="text-[rgb(0,220,129)]">Explore</span>
            </h2>
            <p className="font-normal text-center md:text-lg 2xl:text-xl line-clamp-2 mb-12">
              Discover powerful modules, integrate with your favorite providers
              and start quickly with themes.
            </p>
            <div className="grid grid-cols-1 sm:grid-cols-3 gap-8 pb-8">
              {EXPLORES.map((items) => {
                return (
                  <a
                    key={items.title}
                    className="flex flex-col items-center transition duration-200 p-6 rounded-md hover:bg-[rgb(229,248,255)] 
                    cursor-pointer
                    "
                  >
                    <span className="mb-4 transition duration-200  ">
                      <img src={items.img} />
                    </span>
                    <h3 className="mb-1 text-center text-lg lg:text-xl font-bold">
                      {items.title}
                    </h3>
                    <p className="text-center text-sm lg:text-base mb-4">
                      {items.des}
                    </p>
                  </a>
                );
              })}
            </div>
          </div>
        </div>
      </section>
      <img
        className="absolute left-0 bottom-0 w-full object-fit"
        src="https://nuxtjs.org/img/home/campfire/campfire-illustration.svg"
      />
    </div>
  );
}

export default Explore;
