import React from "react";
import { PARTNERS } from "../../../../../../data/data";

function Partners() {
  return (
    <div className="relative pb-48 sm:pb-56 md:pb-64 lg:pb-72 xl:pb-92 2xl:pb-128 bg-[rgb(229,248,255)]">
      <section className="relative pt-20 z-10">
        <div className="relative w-full d-container-content flex flex-col items-center xl:pt-10 ">
          <div className="flex flex-col w-full items-center col-span-12">
            <div className="mb-2">
              <span className="opacity-[0.6] font-bold text-lg">Partners </span>
            </div>
            <h2
              className="font-normal text-center md:text-left 
 font-serif text-4xl  sm:text-4xl 2xl:text-4xl mb-2"
            >
              Sustainable {""}
              <span className="text-[rgb(0,220,129)]">Development</span>
            </h2>
            <p className="font-normal text-center md:text-lg 2xl:text-xl line-clamp-2 mb-12">
              Nuxt development is carried out by passionate developers, but the
              amount of effort needed to maintain and develop new features is
              not sustainable without proper financial backing. We are thankful
              for our sponsors and partners, who help make Nuxt possible.
            </p>
            <div className="grid grid-cols-1 sm:grid-cols-2 gap-8 md:px-16 pt-16 pb-24">
              <>
                {PARTNERS.map((items) => {
                  return (
                    <div
                      key={items.title}
                      className="flex flex-col space-y-4 items-center text-center"
                    >
                      <img className="w-12 h-12" src={items.img} />
                      <h2 className="font-bold text-md md:text-lg">
                        {items.title}
                      </h2>
                      <p className="text-sm md:text-base px-8 md:px-12 pb-2">
                        {items.des}
                      </p>
                      <a
                        className="p-2 bg-[rgb(229,248,255)] border border-solid 
                      border-black rounded-md cursor-pointer hover:text-stone-700 text-base"
                      >
                        {items.to}
                      </a>
                    </div>
                  );
                })}
              </>
            </div>
            <p>
              <a className="p-2 bg-[rgb(0,220,129)] rounded-md hover:opacity-[0.6] cursor-pointer">
                Become a partner
              </a>
            </p>
          </div>
        </div>
      </section>
      <img
        className="absolute left-0 bottom-0 object-fill w-full -mt-28 sm:-mt-40 lg:-mt-60 xl:-mt-80"
        src="https://nuxtjs.org/img/home/discover/partners/partners-illustration.svg"
      />
    </div>
  );
}

export default Partners;
