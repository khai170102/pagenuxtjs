import React from "react";

function Star() {
  return (
    <div className="absolute left-0 w-full h-full overflow-hidden -top-24 z-0">
      <div className="absolute bg-white rounded-full opacity-100 star top-[300.003px] left-[10.267px] h-[3px] w-[3px] " />
      <div className="absolute bg-white rounded-full opacity-100 star top-[400.003px] left-[200.267px] h-[3px] w-[3px]" />
      <div className="absolute bg-white rounded-full opacity-100 star top-[390.003px] left-[400.267px] h-[3px] w-[3px]" />
      <div className="absolute bg-white rounded-full opacity-100 star top-[280.003px] left-[30.267px] h-[2px] w-[2px]" />
      <div className="absolute bg-white rounded-full opacity-100 star top-[480.003px] left-[70.267px] h-[2px] w-[2px]" />
      <div className="absolute bg-white rounded-full opacity-100 star top-[580.003px] left-[130.267px] h-[3px] w-[3px]" />
      <div className="absolute bg-white rounded-full opacity-100 star top-[850.003px] left-[250.267px] h-[3px] w-[3px]" />
      <div className="absolute bg-white rounded-full opacity-100 star top-[840.003px] left-[20.267px] h-[3px] w-[3px]" />
      <div className="absolute bg-white rounded-full opacity-100 star top-[700.003px] left-[400.267px] h-[3px] w-[3px]" />
      <div className="absolute bg-white rounded-full opacity-100 star top-[610.003px] left-[520.267px] h-[3px] w-[3px]" />
      <div className="absolute bg-white rounded-full opacity-100 star top-[730.003px] left-[770.267px] h-[3px] w-[3px]" />
      //middle
      <div className="absolute bg-white rounded-full opacity-100 star top-[300.003px] left-[700.267px] h-[3px] w-[3px]" />
      <div className="absolute bg-white rounded-full opacity-100 star top-[500.003px] left-[500.267px] h-[3px] w-[3px]" />
      <div className="absolute bg-white rounded-full opacity-100 star top-[700.003px] left-[540.267px] h-[3px] w-[3px]" />
      <div className="absolute bg-white rounded-full opacity-100 star top-[750.003px] left-[800.267px] h-[3px] w-[3px]" />
      <div className="absolute bg-white rounded-full opacity-100 star top-[400.003px] left-[940.267px] h-[3px] w-[3px]" />
      <div className="absolute bg-white rounded-full opacity-100 star top-[460.003px] left-[990.267px] h-[3px] w-[3px]" />
      <div className="absolute bg-white rounded-full opacity-100 star top-[590.003px] left-[1000.267px] h-[3px] w-[3px]" />
      <div className="absolute bg-white rounded-full opacity-100 star top-[720.003px] left-[1020.267px] h-[3px] w-[3px]" />
      //right
      <div className="absolute bg-white rounded-full opacity-100 star top-[300.003px] left-[1100.267px] h-[3px] w-[3px]" />
      <div className="absolute bg-white rounded-full opacity-100 star top-[500.003px] left-[1190.267px] h-[3px] w-[3px]" />
      <div className="absolute bg-white rounded-full opacity-100 star top-[600.003px] left-[1000.267px] h-[3px] w-[3px]" />
      <div className="absolute bg-white rounded-full opacity-100 star top-[800.003px] left-[1140.267px] h-[3px] w-[3px]" />
      <div className="absolute bg-white rounded-full opacity-100 star top-[820.003px] left-[900.267px] h-[3px] w-[3px]" />
      <div className="absolute bg-white rounded-full opacity-100 star top-[250.003px] left-[1300.267px] h-[3px] w-[3px]" />
    </div>
  );
}

export default Star;
