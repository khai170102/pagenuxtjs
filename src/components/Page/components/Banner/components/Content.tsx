import React from "react";
import { AiFillStar, AiFillGithub } from "react-icons/ai";

function Content() {
  return (
    <div className="flex flex-wrap justify-center py-0 section">
      <section className="flex flex-col justify-start w-full px-4 pt-36 pb-48 md:pt-44 lg:pb-56 lg:pt-36 text-center z-20">
        <div className="flex justify-center mb-16">
          <a className="flex items-center truncate bg-[rgb(2,77,62)] hover:bg-[rgb(39,122,106)] text-white border border-[rgb(0,220,129)] py-3 px-4 rounded">
            <AiFillStar className="h-4 w-4 mr-2" />
            <span className="truncate">
              <b>Announcement</b>: Nuxt 3 Release Candidate is out!
            </span>
          </a>
        </div>
        <h1 className="font-medium font-serif text-5xl sm:text-5xl md:text-6xl 2xl:text-6xl mb-6">
          The Intuitive Vue
          <br />
          Framework
        </h1>
        <h2 className="font-normal text-body-base xs:text-lg md:text-xl 2xl:text-2xl mb-8 px-8 sm:px-0 opacity-[0.6] line-clamp-4">
          Build your next Vue.js application with confidence using Nuxt.
          <br className="hidden sm:block" />
          An open source framework making web development simple and powerful.
        </h2>
        <div className="flex flex-col sm:flex-row flex-wrap items-center justify-center space-y-3 sm:space-y-0 sm:space-x-3 xl:space-x-4">
          <a className="text-white rounded-lg bg-[rgb(1,30,38)] p-3 leading-4 medium flex hover:opacity-[0.6] cursor-pointer">
            <AiFillGithub className="mr-2  " />
            40K+ GitHub stars
          </a>
          <a className="rounded-lg bg-[rgb(0,220,129)] p-3 text-black hover:opacity-[0.6] cursor-pointer">
            Get started
          </a>
        </div>
      </section>
    </div>
  );
}

export default Content;
