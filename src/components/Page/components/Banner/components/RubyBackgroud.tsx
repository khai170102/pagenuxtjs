import React from "react";

function RubyBackgroud() {
  return (
    <div
      className="
  absolute
  left-0
  z-10
  w-full
  h-full
  select-none
  pointer-events-none
  transition-opacity
  ease-out
  duration-800
  mt-18
 opacity-100"
    >
      <img
        data-speed="2"
        loading="lazy"
        src="https://nuxtjs.org/img/home/hero/gem-1.svg"
        className="hidden lg:block absolute left-[100px] top-10  md:ml-12 ml-20  md:mt-8 mt-8 "
      />
      <img
        data-speed="-5"
        loading="lazy"
        src="https://nuxtjs.org/img/home/hero/gem-2.svg"
        className="absolute left-1/3 sm:left-auto mt-4 sm:ml-0 sm:right-0 top-0 sm:mr-20 lg:mr-60 lg:mt-20"
      />
      <img
        data-speed="1"
        loading="lazy"
        src="https://nuxtjs.org/img/home/hero/gem-3.svg"
        className="hidden lg:block absolute right-0 top-1/4"
      />
      <img
        data-speed="-3"
        loading="lazy"
        src="https://nuxtjs.org/img/home/hero/gem-4.svg"
        className="absolute right-0 bottom-0 mb-20 sm:mb-0 sm:bottom-1/4 mr-8 sm:mr-16 md:mr-28 lg:mr-40"
      />
      <img
        data-speed="-5"
        loading="lazy"
        src="https://nuxtjs.org/img/home/hero/gem-5.svg"
        className="absolute left-0 bottom-0 sm:bottom-1/4 mb-20 sm:mb-0 ml-0 md:ml-12 lg:ml-40"
      />
      <img
        data-speed="-2"
        loading="lazy"
        src="https://nuxtjs.org/img/home/hero/gem-2.svg"
        className="hidden sm:block absolute left-20 top-0 mt-20 lg:top-1/3 lg:-mt-2 rotate-45"
      />
    </div>
  );
}

export default RubyBackgroud;
