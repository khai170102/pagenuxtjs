import React from "react";
import Content from "./components/Content";
import RubyBackgroud from "./components/RubyBackgroud";
import Star from "./components/Star";

function Banner() {
  return (
    <div className="overflow-hidden h-full relative  bg-[rgb(1,30,38)] text-white -mt-14 pt-14 md:-mt-18 md:pt-18">
      <RubyBackgroud />
      <Star />
      <Content />
    </div>
  );
}

export default Banner;
