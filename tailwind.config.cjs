module.exports = {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    extend: {},
    screen: {},
  },
  plugins: [require("@tailwindcss/line-clamp")],
};
